# HUM216

Quentin Lieumont

## Questions de cours

### Quelle place l'entreprise Olivetti accorde-t'elle au design au cours de son histoire ?

L'entreprise Olivetti accorde une place importante au design au cours de son histoire.
On peut par exemple noter la conception de l'usine de production de machines à écrire,
disopsée de telle manière que les employés de l'entreprise puissent se concentrer sur leur travail
grâce a l'arivée de lumiere tout en apportant un environement sain de travail via
la création de champs cultivé par les employés eux mêmes.

### Comment Charles et Ray Eames cherchent-ils à modifier l'image de marque de l'entreprise IBM à partir de la fin des années 1950 ?

Durant la guere froide, IBM a soutenue les états-unis contre le block soviétique.
Commancant par orienter leur comunication vers la patrie et la liberté,
quand la population est devenue hostile au conflit,
IBM a décidé de recentrer ca publicité vers le progres de la société et l'augmentation des conaissnaces.
Ils on donc fait appels à Charles et Ray Eames pour leur comander des films pour redorer leur image.
De plus, ils on réalisé une exposition: "Mathematica: A World of Numbers... and Beyond" en 1961
au musée des sciences de californie.
Grâce a ces actions, IBM a réussit à se faire une image de marque plus moderne, orienté vers l'accès a l'information.

## Composition

### Commentaire de l'extrait suivant issu de l'étude Portrait de l'ingénieur 2030 produite en 2014 par l'Institut Mines – Telecom

#### Portrait de l’ingénieur 2030 : une introspection

L’Institut Mines-Télécom forme des ingénieurs de haut niveau au sein d’un ensemble
diversifié d’écoles couvrant un large spectre de spécialités.
Le Portrait de l’ingénieur 2030 est un document qui a pour but de décrire les compétences
dans un monde en évolution rapide.

En effet, depuis l'avenement de l'inforatique et de la globalisation, le monde évolue de maniere exponentielle.
L'Institut doit donc de plus en plus régulierement réévaluer ses enseignement et sa place dans un monde complexe.

Conscient des défis à venir, l’Institut s’est interrogé sur les compétences transverses
que l’ensemble de ses écoles devraient s’attacher à livrer dans les années qui viennent,
afin d’assurer à ses ingénieurs, non seulement un bon départ professionnel mais
également une capacité pérenne d’adaptation au monde de demain.

#### Des défis à venir

La periode actuelle est particulierement complexe de part le nombre et la diversité des problèmes futurs :

- La crise écologique: l'utilisation des énergies fosiles à dors et déjà créée une dérive climatique qui va mener à des boulversements géopolitiques et économiques majeurs.
- La crise énergetique: les resources d'énergies fosiles sont en forte décroissance.
- La crise sociale : les démocraties partout dans le monde sont en pleine crise existantielle les défis de demains doivent donc etre pris en compte avec leur dimension sociale.
- Les crises économiques: les problèmes précédents ont une influance sur les économies et vont donc mener a des crise économique de plus en plus violantes.
- La crise de la surveillance : les données personnelles sont actuellement peu protégées par les loies, de grands groupes profitent donc de ce flou jurique pour accumuler des bénéfices au détriment des de la vie des gens.

#### La résilience : une alternative à l'efficacité

Depuis le début de l'ère industrielle, l'efficacité à été une priorité pour les entreprises comme pour la formation.
Cependant, à la lumiere des défis à venir, l'efficacité ne parait plus l'élément central,
car une trop forte optimisation sous contraintes fait perdre en généralité de la solution.

De ce fait les process trop efficaces perdent en résiliances et donc
ne sont plus à même de répondre aux besoins lors de variations d'environements.
Il faut donc faire un travail d'arbitrage entre efficacité et résilience.

Aujourd'hui la résilience est considéré comme essentielle durant la formation.

Pour obtenir des process résiliant, il faut donc avoir prévus les potentielles erreurs et perturbations auquel il peut être confronté.
De ce fait une equipe plus ouverte et diversifiée est un point de départ intéressant.
Des ingénieurs ayant une culture transversale peuvent plus facilement prévoir des scénarios de perturbations multiples et donc les anticiper.

L’Institut Mines-Télécom doit donc réusir a diversifier sa formation.
Cette logique explique pourquoi les écoles d'ingénieurs mettent un point d'honneur à engager des parcours de plus en plus diférents : étudiants internationaux, étudiants en situation de handicap ou quotats de femmes dans les étudiants en sont les examples les plus frapants.

#### Le design : un moyen d'accelerer les hibridations

Dans ce rapport, il est aussi mis en évidence que le design est positif pour la conception et la réalisation d'un processus.

par exemple les hackerspaces sont des endroits ou un grand mélange de cultures s'oppère.
On retrouve cette mentalité dans les Fablabs ou le design est un point central de la conception.

Les jeunes générations se tournent de plus en plus vers cette logique d'apprendre par la pratique,
methodes privilégiées autrefois impossible a mettre en oeuvre durant la formation d'ingénieurs industriels.
Grâce au nouvelles évolutions de la formation d'ingénieurs, il est bien plus simple aujourd'hui d'apprendre par soi même.

La formation d'ingénieurs a donc un double défi a relever : Réusir à faire gagner une culture générale diversifiée à ses ingénieurs et faire évoluer la formation pour former des ingénieurs de haut niveau sur des sujets de plus en plus complexes et interdépendants.

#### Conclusion

Ces diférentes évolutions de la formation d'ingénieurs ont donc un impact direct sur les étudiants.
On peut par exemple noter la diversité des cours de formations humaines proposés aux étudiants
pour leur permetre de sair les multiples complexités auquels ils font face.

D'un autre coté les cours de spécialités évoluent pour par exemple faire place a la filière GIN
qui inclus les cours de plusieurs autres filieres afin de former un profil bien plus complet et transversal.

Enfin les parcours alternatifs comme les années de césure ou les alternances se développent et
sont de plus en plus porposées aux nouveaux étudiants.
